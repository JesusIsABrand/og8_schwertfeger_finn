package primzahl;

public class Primzahl {

    public boolean isPrim(long zahl) {
        if (zahl <2) {
            return false;
        }
        for (long i=2; i<zahl; i++) {
            if (zahl%i==0) {
                return false;
            }
        }
    return true;
    }

    public static void main(String[] args) {

        long eineZahl = 9_999_999_929L;
        Stoppuhr stopwatch = new Stoppuhr();
        Primzahl pt = new Primzahl();
        stopwatch.start();
        System.out.println(pt.isPrim(eineZahl));
        stopwatch.stop();
        System.out.println(stopwatch.getDauerInMs() + "ms");

    }
}
