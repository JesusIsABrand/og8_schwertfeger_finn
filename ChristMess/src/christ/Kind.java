package christ;

import java.util.*;

import javax.print.attribute.standard.MediaSize.Other;

public class Kind implements Comparable {
	//Jedes Kind hat einen Nachnamen, Vornamen, Geburtstag, Wohnort und Bravheitsgrad
	//Erstellen Sie einen vollparametrisierten Konstruktor, Getter/Setter und eine toString-Methode
	
	private String vorname, nachname, geburtsdatum, ort;
	public int bravheitsgrad;
	
	public Kind(String vorname, String nachname, String geburtsdatum, int bravheitsgrad, String ort) {
		this.vorname = vorname;
		this.nachname = nachname;
		this.geburtsdatum = geburtsdatum;
		this.bravheitsgrad = bravheitsgrad;
		this.ort = ort;
	}
	
	public String getVorname() {
		return vorname;
	}
	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	public String getNachname() {
		return nachname;
	}
	public void setNachname(String nachname) {
		this.nachname = nachname;
	}
	public String getGeburtsdatum() {
		return geburtsdatum;
	}
	public void setGeburtsdatum(String geburtsdatum) {
		this.geburtsdatum = geburtsdatum;
	}
	public int getBravheitsgrad() {
		return bravheitsgrad;
	}
	public void setBravheitsgrad(int bravheitsgrad) {
		this.bravheitsgrad = bravheitsgrad;
	}
	public String getOrt() {
		return ort;
	}
	public void setOrt(String ort) {
		this.ort = ort;
	}

	@Override
	public String toString() {
		return "Kind [vorname=" + vorname + ", nachname=" + nachname + ", geburtsdatum=" + geburtsdatum
				+ ", bravheitsgrad=" + bravheitsgrad + ", ort=" + ort + "]";
	}

	@Override
	public int compareTo(Object o) {
		return this.nachname.compareTo(nachname);
	}
}
