package zootycoon;

import java.util.Scanner;

public class Addon {

    private int idnummer;
    private double verkaufspreis;
    private String bezeichnung;
    private int anzaddon;
    private int maxaddon;

    public Addon(int idnummer, double verkaufspreis, String bezeichnung, int anzaddon, int maxaddon) {
        this.idnummer = idnummer;
        this.verkaufspreis = verkaufspreis;
        this.bezeichnung = bezeichnung;
        this.anzaddon = anzaddon;
        this.maxaddon = maxaddon;
    }

    public int getIdnummer() {
        return idnummer;
    }

    public void setIdnummer(int idnummer) {
        this.idnummer = idnummer;
    }

    public double getVerkaufspreis() {
        return verkaufspreis;
    }

    public void setVerkaufspreis(double verkaufspreis) {
        this.verkaufspreis = verkaufspreis;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    public int getAnzaddon() {
        return anzaddon;
    }

    public void setAnzaddon(int anzaddon) {
        this.anzaddon = anzaddon;
    }

    public int getMaxaddon() {
        return maxaddon;
    }

    public void setMaxaddon(int maxaddon) {
        this.maxaddon = maxaddon;
    }

    public void aendereBestand(){


    }
}